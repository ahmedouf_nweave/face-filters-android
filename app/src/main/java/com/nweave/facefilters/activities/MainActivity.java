package com.nweave.facefilters.activities;

import android.os.Bundle;

import com.nweave.facefilters.R;
import com.nweave.facefilters.adapters.FacesAdapter;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

public class MainActivity extends AppCompatActivity {
    private RecyclerView facesRecyclerView;
    private FacesAdapter facesAdapter;
    private ArrayList<String> facesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        facesList.add("black");
        facesList.add("vandetta");
        facesList.add("osloopi");
        facesList.add("hulk");
        facesList.add("stone");
        facesList.add("joker");
        facesRecyclerView = findViewById(R.id.faces_recyclerview);
        OverScrollDecoratorHelper.setUpOverScroll(facesRecyclerView, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
        facesAdapter = new FacesAdapter(facesList,this);
        facesRecyclerView.setAdapter(facesAdapter);
    }
}
