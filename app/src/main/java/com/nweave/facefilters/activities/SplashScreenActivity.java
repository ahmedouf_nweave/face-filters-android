package com.nweave.facefilters.activities;

import androidx.appcompat.app.AppCompatActivity;
import gr.net.maroulis.library.EasySplashScreen;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.nweave.facefilters.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EasySplashScreen easySplashScreenView = new EasySplashScreen(SplashScreenActivity.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(2500)
                .withBackgroundResource(android.R.color.black)
                .withLogo(R.drawable.splash_icon);

        View splashScreen = easySplashScreenView.create();

        setContentView(splashScreen);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Animatoo.animateFade(this);
    }
}
