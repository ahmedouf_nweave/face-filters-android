package com.nweave.facefilters.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.nweave.facefilters.R;
import com.nweave.facefilters.activities.CameraViewActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FacesAdapter extends RecyclerView.Adapter<FacesAdapter.FacesHolder> {

    private ArrayList<String> facesArray = new ArrayList<>();
    private Context context;

    public FacesAdapter(ArrayList<String> facesArray, Context context) {
        this.facesArray = facesArray;
        this.context = context;
    }

    @NonNull
    @Override
    public FacesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater myInflater = LayoutInflater.from(parent.getContext());
        View facesView = myInflater.inflate(R.layout.face_view, parent, false);
        return new FacesHolder(facesView);
    }

    @Override
    public void onBindViewHolder(@NonNull FacesHolder holder, int position) {
        final String string = facesArray.get(position);
        int id = context.getResources().getIdentifier(string, "drawable", context.getPackageName());
        holder.faceImageView.setImageResource(id);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(context , CameraViewActivity.class);
                cameraIntent.putExtra("face", string);
                context.startActivity(cameraIntent);
                Animatoo.animateInAndOut(context);
            }
        });
    }

    @Override
    public int getItemCount() {
        return facesArray.size();
    }

    public class FacesHolder extends RecyclerView.ViewHolder {
        private ImageView faceImageView;

        public FacesHolder(@NonNull View itemView) {
            super(itemView);
            faceImageView = itemView.findViewById(R.id.face_cell_imageView);
        }
    }
}
